//
//  AjFlutterAppspPushPlugin.h
//  PushDemo
//
//  Created by zfl
//  Copyright © 2018 anji-plus 安吉加加信息技术有限公司 http://www.anji-plus.com. All rights reserved.
//

#import <Flutter/Flutter.h>

@interface AjFlutterAppspPushPlugin : NSObject<FlutterPlugin>

@property (nonatomic, copy) NSString *host;
@property (nonatomic, copy) NSString *appspAppKey;
@property (nonatomic, copy) NSString *appspSecretKey;
@property (nonatomic, copy) NSString *jpushAppKey;
@property (nonatomic, copy) NSString *jchannel;

@property FlutterMethodChannel *channel;

+ (void)setDataWithDic:(NSDictionary *)dic;
@end
