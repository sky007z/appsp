package com.anji.sp.push.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 服务推送id记录
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PushMessageHistoryVO implements Serializable {

    private static final long serialVersionUID=1L;
    /** 应用唯一key */
    private String appKey;

    /** 消息唯一id */
    private String msgId;
    /**
     * 目标（对应消息推送总数）
     */
    private String targetNum;

    /**
     * 成功条数（对应消息推送成功总数）
     */
    private String successNum;

    /** 消息内容消息*/
    private String title;
    private String content;
    private String extras;
    private String iosConfig;
    private String androidConfig;

    /** 消费状态 默认未消费 0： 未消费 1：已消费 */
    private Integer consumptionState;

    /**
     * 推送类型 1透传消息 0 普通消息
     */
    private String pushType;

    /**
     * 历史详情
     */
    private List details;

}
