package com.anji.sp.push.model.vo;

import java.io.Serializable;

/**
 * Oppo推送回调Bean
 */
public class OppoCallBackBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private String messageId;
    private String taskId;
    private String registrationIds;//"regId1,regId2,regId3"
    private String param;
    private String eventType;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getRegistrationIds() {
        return registrationIds;
    }

    public void setRegistrationIds(String registrationIds) {
        this.registrationIds = registrationIds;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
    /**
     * [
     *     {
     *         "messageId": "msgId1",
     *         "taskId": "taskId1",
     *         "registrationIds": "regId1, regid2",
     *         "param": "call_back_parameter",
     *         "eventType": "push_arrive"
     *     },
     *     {
     *         "messageId": "msgId1",
     *         "taskId": "taskId1",
     *         "registrationIds": "regId1,regid2",
     *         "param": "call_back_parameter",
     *         "eventType": "push_arrive"
     *     }
     * ]
     */
}
