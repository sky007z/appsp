package com.anji.sp.push.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 服务推送id记录
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("push_message")
public class PushMessagePO implements Serializable {

    private static final long serialVersionUID=1L;

    /** 编号 */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 应用唯一key */
    private String appKey;

    /** 消息唯一id */
    private String msgId;
    /**
     * 目标（对应消息推送总数）
     */
    private String targetNum;

    /**
     * 成功条数（对应消息推送成功总数）
     */
    private String successNum;

    /** 消息内容消息json */
    private String operParam;

    /** 消费状态 默认未消费 0： 未消费 1：已消费 */
    private Integer consumptionState;

    /**
     * 消息来源 0 web 1 api
     */
    private String sendOrigin;

    /**
     * 推送类型 1透传消息 0 普通消息
     */
    private String pushType;

    /** 0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG */
    private Integer enableFlag;

    /**  0--未删除 1--已删除 DIC_NAME=DEL_FLAG */
    private Integer deleteFlag;

    /** 创建者 */
    private Long createBy;

    /** 创建时间 */
    private LocalDateTime createDate;

    /** 更新者 */
    private Long updateBy;

    /** 更新时间 */
    private LocalDateTime updateDate;

    /** 备注信息 */
    private String remarks;


}
