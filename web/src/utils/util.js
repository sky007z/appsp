import store from '@/store'

// 字符串去空格
export function removeSpaces(str) {
	return str.replace(/\s/g,"");
}

// 字符串提取：支持提取中文、英文、数字
// type: 0-提取中文 1-提取英文 2-提取数字
// needSpace: 是否需要空格，默认false
export function pickUpString(value, type = 0, needSpace = false) {
    if(type == 0) {
        if(needSpace == false) {
            return value.replace(/[^\u4E00-\u9FA5]/g,'')
        }else {
            return value.replace(/[^\u4E00-\u9FA5\s]/g,'')
        }
    }else if (type == 1) {
        if(needSpace == false) {
            return value.replace(/[^a-zA-Z]/g,'')
        }else {
            return value.replace(/[^a-zA-Z\s]/g,'')
        }
    }else if(type == 2) {
        if(needSpace == false) {
            return value.replace(/[^\d]/g,'')
        }else {
            return value.replace(/[^\d\s]/g,'')
        }
    }
}

// 克隆对象
export function copyObj(base) {
    var c = {};
	c = JSON.parse(JSON.stringify(base));
	return c;
}

/**
 * @param {Array} value
 * @returns {Boolean}
 * @example see @/views/permission/directive.vue
 */
export function checkPermission(value) {
	if (value && value instanceof Array && value.length > 0) {
		const roles = store.getters && store.getters.roles
		const permissionRoles = value

		const hasPermission = roles.some(role => {
		return permissionRoles.includes(role)
		})
		return hasPermission
	} else {
		console.error(`need roles! Like v-permission="['admin','editor']"`)
		return false
	}
}

// 判断是否为utf-8
export function isUTF8(bytes) {
    var i = 0;
    while (i < bytes.length) {
        if ((// ASCII
            bytes[i] == 0x09 ||
            bytes[i] == 0x0A ||
            bytes[i] == 0x0D ||
            (0x20 <= bytes[i] && bytes[i] <= 0x7E)
        )
        ) {
            i += 1;
            continue;
        }

        if ((// non-overlong 2-byte
            (0xC2 <= bytes[i] && bytes[i] <= 0xDF) &&
            (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0xBF)
        )
        ) {
            i += 2;
            continue;
        }

        if ((// excluding overlongs
            bytes[i] == 0xE0 &&
            (0xA0 <= bytes[i + 1] && bytes[i + 1] <= 0xBF) &&
            (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF)
        ) ||
            (// straight 3-byte
                ((0xE1 <= bytes[i] && bytes[i] <= 0xEC) ||
                    bytes[i] == 0xEE ||
                    bytes[i] == 0xEF) &&
                (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0xBF) &&
                (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF)
            ) ||
            (// excluding surrogates
                bytes[i] == 0xED &&
                (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0x9F) &&
                (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF)
            )
        ) {
            i += 3;
            continue;
        }

        if ((// planes 1-3
            bytes[i] == 0xF0 &&
            (0x90 <= bytes[i + 1] && bytes[i + 1] <= 0xBF) &&
            (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF) &&
            (0x80 <= bytes[i + 3] && bytes[i + 3] <= 0xBF)
        ) ||
            (// planes 4-15
                (0xF1 <= bytes[i] && bytes[i] <= 0xF3) &&
                (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0xBF) &&
                (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF) &&
                (0x80 <= bytes[i + 3] && bytes[i + 3] <= 0xBF)
            ) ||
            (// plane 16
                bytes[i] == 0xF4 &&
                (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0x8F) &&
                (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF) &&
                (0x80 <= bytes[i + 3] && bytes[i + 3] <= 0xBF)
            )
        ) {
            i += 4;
            continue;
        }
        return false;
    }
    return true;
}

// 从对象取值如果为null返回空字符串
export function getValueFromObj(obj, key) {
    var value = obj[key]
    if (value == null || value == undefined) {
        return ''
    }
    return value
}

// val保留len位的小数，不四舍五入，例 toDecimal(1.599999, 3) -> 1.599
export function toDecimal(val,len) {
    var f = parseFloat(val);
    if (isNaN(f)) {
        return '';
    }
    var s=val.toString();
    if(s.indexOf(".")>0){
        var f = s.split(".")[1].substring(0,len)
        s=s.split(".")[0]+"."+f
    }
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        if(len != 0) {
            s += '.';
        }
    }
    while (s.length <= rs + len && len != 0) {
        s += '0';
    }
    return s;
}

// 数组转字符串[1,2,3]=>"1,2"
export function acTiveArrStringFun(obj) {
    var arr = [];
    if (obj != null && obj.length != 0) {
        for (var i = 0; i < obj.length; i++) {
            arr.push(obj[i]);
        }
    }
    return arr.toString();
}

// 将数组array分成长度为subGroupLength的小数组并返回新数组，例：array = [1,2,3,4,5,6,7], subGroupLength = 3 -> [[1,2,3], [4,5,6], [7]]
export function groupArr(array, subGroupLength) {
    let index = 0;
    let newArray = [];
    while(index < array.length) {
        newArray.push(array.slice(index, index += subGroupLength));
    }
    return newArray;
}

// 阿拉伯数字转中文数字
export function numToChinese(num) {
    if (!/^\d*(\.\d*)?$/.test(num)) {
        alert("Number is wrong!");
        return "Number is wrong!";
    }
    var AA = new Array("零", "一", "二", "三", "四", "五", "六", "七", "八", "九");
    var BB = new Array("", "十", "百", "千", "万", "亿", "点", "");
    var a = ("" + num).replace(/(^0*)/g, "").split("."),
        k = 0,
        re = "";
    for (var i = a[0].length - 1; i >= 0; i--) {
        switch (k) {
            case 0:
                re = BB[7] + re;
                break;
            case 4:
                if (!new RegExp("0{4}\\d{" + (a[0].length - i - 1) + "}$").test(a[0]))
                    re = BB[4] + re;
                break;
            case 8:
                re = BB[5] + re;
                BB[7] = BB[5];
                k = 0;
                break;
        }
        if (k % 4 == 2 && a[0].charAt(i + 2) != 0 && a[0].charAt(i + 1) == 0) re = AA[0] + re;
        if (a[0].charAt(i) != 0) re = AA[a[0].charAt(i)] + BB[k % 4] + re;
        k++;
    }
    if (a.length > 1) //加上小数部分(如果有小数部分) 
    {
        re += BB[6];
        for (var i = 0; i < a[1].length; i++) re += AA[a[1].charAt(i)];
    }
    return re;
}

// 判断是否手机端
export function isMobile() {
    let flag = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)
    return flag;
}